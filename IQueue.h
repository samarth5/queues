#ifndef IQUEUE_H
#define IQUEUE_H

template <class T>
class IQueue
{
    public:
        
        virtual ~IQueue()
        {

        }

        virtual void Enqueue(T elementToQueue) = 0;
        virtual T Dequeue() = 0;
        virtual IQueue<T>* GetInstance() = 0;
};

#endif