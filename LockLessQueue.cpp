// Deriving from IQueue<char*> => LockLessQueue is not a class template
// Deviation from usual IQueue based queues => call create() first and foremost
// TODO Find correct parameters for our test cases to work 
#include "MPSCQueue.h"

using namespace std;


class LockLessQueue : public IQueue<char*>
{
    private:
        MPSCQueue queueInstance;
    
    public:
        
        // To allocate memory dynamically
        // default constructor
        LockLessQueue()
        {
            queueInstance = MPSCQueueCreate();
        } 
        
        // parametric constructor
        LockLessQueue(int nodeSize, int maxNode, int queuebound, int nodeWrap, int queueWrap)
        {
            DataNodeSize = nodeSize;
            MaxDataNodes = maxNode;
            QueueUpperBound = queuebound;
            DataNodeWrap = nodeWrap;
            QueueWrap = queueWrap; 
            queueInstance = MPSCQueueCreate();
        }

        virtual void Enqueue(char* elementToQueue)
        {
            MPSCQueuePush(queueInstance, elementToQueue);
            
        }
        // returns NULL if frontElement not found
        virtual char* Dequeue()
        {
            char* frontElement = MPSCQueuePop(queueInstance);
            return frontElement;
        }

        virtual IQueue<char*>* GetInstance()
        {       
            return new LockLessQueue();
        }
        
    
};
