#ifndef MEASUREMENTS_H
#define MEASUREMENTS_H

#include "IQueue.h"
#include <iostream>
#include <algorithm>
#include <memory>
#include <thread>

using namespace std;

class Measure
{
    public:      
        vector<double> values;
        
        Measure()
        {            
        }

        Measure(vector<double> &values)
        {
            this->values = values;
        }
};

template<typename T>
using EnqueueOperationFunction = void (*)(IQueue<T> &, int);

template<typename T>
using DequeueOperationFunction = void (*)(IQueue<T> &, int, int);

template<class T>
class Operation
{
    public:
 
        int NumberOfThreads;
        int NumberOfEnqueueRepititions;
        int NumberOfDequeueRepititions;
        int NumberOfMicroSecondsToDequeueFor;
        shared_ptr<IQueue<T>> BaseQueuePointer;
        
        EnqueueOperationFunction<T> EnqueueOperation;
        DequeueOperationFunction<T> DequeueOperation;
        
        Operation(shared_ptr<IQueue<T>> baseQueuePointer, EnqueueOperationFunction<T> enqueueOperation, 
            int numberOfEnqueueRepititions, int numberOfThreads = 100000)
        {
            this->EnqueueOperation = enqueueOperation;
            this->NumberOfThreads =  numberOfThreads > thread::hardware_concurrency() ? thread::hardware_concurrency() : numberOfThreads;
            this->NumberOfEnqueueRepititions = numberOfEnqueueRepititions;
            this->BaseQueuePointer = baseQueuePointer;
        }

        Operation(shared_ptr<IQueue<T>> baseQueuePointer, EnqueueOperationFunction<T> enqueueOperation, 
            int numberOfEnqueueRepititions, DequeueOperationFunction<T> dequeueOperation,
            int numberOfDequeueRepititions, int numberOfMicroSecondsToDequeueFor)
        {
            this->EnqueueOperation = enqueueOperation;
            this->DequeueOperation = dequeueOperation;
            this->NumberOfThreads = thread::hardware_concurrency();
            this->NumberOfEnqueueRepititions = numberOfEnqueueRepititions;
            this->NumberOfDequeueRepititions = numberOfDequeueRepititions;
            this->BaseQueuePointer = baseQueuePointer;
            this->NumberOfMicroSecondsToDequeueFor = numberOfMicroSecondsToDequeueFor;
        }
};

template <class T>
class OperationGroup
{
    public:
        vector<Operation<T>> Operations;
        string OperationDescription;
        bool IsOnlyEnqueueOperation;
        string OutputFileName;

        OperationGroup()
        {

        }
        
        OperationGroup(const string &outputFileName, const string &operationDescription, const bool isOnlyEnqueueOperation)
        {
            this->OutputFileName = outputFileName;
            this->OperationDescription = operationDescription;
            this->IsOnlyEnqueueOperation = isOnlyEnqueueOperation;   
        }
};

inline ostream& operator<<(ostream &strm, const Measure &measure) 
{
    for (int i = 0; i < measure.values.size() - 1; i++)
    {
        strm << measure.values[i] << ",";
    }

    strm << measure.values[measure.values.size() - 1] << endl;

    return strm;
}

#endif