#include "Measurements.h"
#include "LockQueue.cpp"
#include "Benchmark.cpp"
#include "LockLessQueue.cpp"
#include "BoostQueue.cpp"
#include "CharmQueue.cpp"
#include "TbbQueue.cpp"
#include "MoodyCamelQueue.cpp"

#include <fstream>
#include <cxxabi.h>
#include <iostream>
#include <math.h>
#include <typeinfo>

using namespace std;

#define quote(x) #x

char* StringToEnqueue = "Hello!";

template <class T>
void EnqueueNItems(IQueue<T> &queue, int n)
{
    for (int i = 0; i < n; i++)
    {
        queue.Enqueue(StringToEnqueue);
    }
}

template <class T>
void DequeueNItems(IQueue<T> &queue, int n, int count)
{    
    while (count--)
    {
        for (int i = 0; i < n; i++)
        {
            queue.Dequeue();
        }

        this_thread::sleep_for(microseconds(1));
    }
}
template <class T>
vector< shared_ptr <IQueue<T>> > getQueues()
{
    // // LockLessQueue
    int DataNodeSize_local = 2048; 
    int MaxDataNodes_local = 2048; 
    int QueueUpperBound_local = DataNodeSize_local * MaxDataNodes_local; 
    int DataNodeWrap_local = DataNodeSize_local - 1; 
    int QueueWrap_local = QueueUpperBound_local - 1;

    vector< shared_ptr <IQueue<T>> > queueList;
    
    queueList.push_back(make_shared<LockQueue<char*>>());
    queueList.push_back(make_shared<LockLessQueue>(DataNodeSize_local, MaxDataNodes_local, 
        QueueUpperBound_local, DataNodeWrap_local, QueueWrap_local));
    queueList.push_back(make_shared<CharmQueue>());
    queueList.push_back(make_shared<TbbQueue<char*>>());
    queueList.push_back(make_shared<MoodyCamelQueue<char*>>());

    return queueList;
}

template <class T>
vector<OperationGroup<T>> getOperationGroup()
{
    int numberOfMicrosecondsToDequeueFor = 10; 
    int numberOfItemsToEnqueueSmall = 100;
    int numberOfItemsToEnqueueLarge = 100000;
    int numberOfItemsToDequeueSmall = 20;
    int numberOfItemsToDequeueLarge = 10000;
    int oneThread = 1;
    bool isOnlyEnqueueOperation = true;
    
    int maxThreads = thread::hardware_concurrency();
    
    vector<OperationGroup<T>> allOperations;

    string operation0Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueSmall)
        + "\tNumberOfThreads: 1\n";
    string operation1Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueLarge)
        + "\tNumberOfThreads: " + to_string(1);
    string operation2Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueSmall)
        + "\tNumberOfThreads: " + to_string(maxThreads);
    string operation3Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueLarge)
        + "\tNumberOfThreads: " + to_string(maxThreads);
    string operation4Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueSmall)
        +  "\tItemsDequeued: " + to_string(numberOfItemsToDequeueSmall) + "\tNumberOfThreads: " + 
        to_string(maxThreads) + "\tDequeueIntervalMicroSeconds: " + to_string(numberOfMicrosecondsToDequeueFor);
    string operation5Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueLarge)
        +  "\tItemsDequeued: " + to_string(numberOfItemsToDequeueSmall) + "\tNumberOfThreads: " + 
        to_string(maxThreads) + "\tDequeueIntervalMicroSeconds: " + to_string(numberOfMicrosecondsToDequeueFor);
    string operation6Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueSmall)
        +  "\tItemsDequeued: " + to_string(numberOfItemsToDequeueLarge) + "\tNumberOfThreads: " + 
        to_string(maxThreads) + "\tDequeueIntervalMicroSeconds: " + to_string(numberOfMicrosecondsToDequeueFor);
    string operation7Description = "ItemsEnqueued per thread: " + to_string(numberOfItemsToEnqueueLarge)
        +  "\tItemsDequeued: " + to_string(numberOfItemsToDequeueLarge) + "\tNumberOfThreads: " + 
        to_string(maxThreads) + "\tDequeueIntervalMicroSeconds: " + to_string(numberOfMicrosecondsToDequeueFor);
    
    allOperations.push_back(OperationGroup<T>("1.txt", operation0Description, isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("2.txt", operation1Description, isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("3.txt", operation2Description, isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("4.txt", operation3Description, isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("5.txt", operation4Description, !isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("6.txt", operation5Description, !isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("7.txt", operation6Description, !isOnlyEnqueueOperation));
    allOperations.push_back(OperationGroup<T>("8.txt", operation7Description, !isOnlyEnqueueOperation));

    for (auto queue : getQueues<T>())
    {
        allOperations[0].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueSmall, 
            oneThread));
        allOperations[1].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueLarge, 
            oneThread));
        allOperations[2].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueSmall));
        allOperations[3].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueLarge));
        allOperations[4].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueSmall,
            DequeueNItems, numberOfItemsToDequeueSmall, numberOfMicrosecondsToDequeueFor));
        allOperations[5].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueLarge,
            DequeueNItems, numberOfItemsToDequeueSmall, numberOfMicrosecondsToDequeueFor));
        allOperations[6].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueSmall,
            DequeueNItems, numberOfItemsToDequeueLarge, numberOfMicrosecondsToDequeueFor));
        allOperations[7].Operations.push_back(Operation<T>(queue, EnqueueNItems, numberOfItemsToEnqueueLarge,
            DequeueNItems, numberOfItemsToDequeueLarge, numberOfMicrosecondsToDequeueFor));
    }

    return allOperations;
}

void WriteNumMeasuresToFile(int numberOfMeasurements)
{
    ofstream numMeasuresFile;
    numMeasuresFile.open("numMeasures.txt");
    numMeasuresFile<< numberOfMeasurements;
    numMeasuresFile.close();
}

void WriteMeasurementsToFile(int numberOfMeasurements)
{
    Benchmark<char*> benchmark;

    for (auto operationGroup : getOperationGroup<char *>())
    {
        ofstream fileHandle;
        
        fileHandle.open(operationGroup.OutputFileName);
        fileHandle << operationGroup.OperationDescription << endl;
        int operationNumber = 1;
        for (auto operation : operationGroup.Operations)
        {
            fileHandle << operationNumber++ << "\t" << 
                benchmark.GetBenchmarkMeasure(operation, operationGroup.IsOnlyEnqueueOperation,
                    numberOfMeasurements);
        }
        
        fileHandle.close();
    }   
}

int main()
{
    int numberOfMeasurements = 10;
    
    WriteNumMeasuresToFile(numberOfMeasurements);
    WriteMeasurementsToFile(numberOfMeasurements);
}
