#include "IQueue.h"
#include "concurrentqueue/concurrentqueue.h"
#include <iostream>

using namespace moodycamel;

template <class T>
class MoodyCamelQueue : public IQueue<T>
{
    private:
        ConcurrentQueue<T> queueInstance;

    public:
        
        virtual void Enqueue(T elementToQueue)
        {
            queueInstance.enqueue(elementToQueue);
        }

        virtual T Dequeue()
        {
            T frontElement;

            if(queueInstance.try_dequeue(frontElement))
            {
                return frontElement;
            }

	        return NULL;
        }

        virtual IQueue<T>* GetInstance()
        {
            return new MoodyCamelQueue<T>();
        }
};
