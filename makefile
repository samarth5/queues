CXX = g++
CXXFLAGS = -Wall -w -g -std=c++11 -pthread
TBB_LIB_RELEASE = ${PWD}/tbb-2018

BenchmarkingModule: BenchmarkingModule.o Benchmark.o LockQueue.o BoostQueue.o TbbQueue.o MoodyCamelQueue.o Measurements.h
	$(CXX) $(CXXFLAGS) -o BenchmarkingModule BenchmarkingModule.o LockQueue.o BoostQueue.o TbbQueue.o MoodyCamelQueue.o Benchmark.o -L$(TBB_LIB_RELEASE) -ltbb


BenchmarkingModule.o: BenchmarkingModule.cpp LockQueue.cpp LockLessQueue.cpp CharmQueue.cpp BoostQueue.cpp TbbQueue.cpp Benchmark.cpp Measurements.h
	$(CXX) $(CXXFLAGS) -c BenchmarkingModule.cpp

TbbQueue.o: TbbQueue.cpp IQueue.h ./tbb-2018/tbb/concurrent_queue.h
	$(CXX) $(CXXFLAGS) -c TbbQueue.cpp -Wl,-rpath,$(TBB_LIB_RELEASE) -L$(TBB_LIB_RELEASE) -ltbb

MoodyCamelQueue.o: MoodyCamelQueue.cpp ./concurrentqueue/concurrentqueue.h IQueue.h
	$(CXX) $(CXXFLAGS) -c MoodyCamelQueue.cpp

BoostQueue.o: BoostQueue.cpp IQueue.h ./Boost/lockfree/queue.hpp
	$(CXX) $(CXXFLAGS) -c BoostQueue.cpp

CharmQueue.o: CharmQueue.cpp IQueue.h PCQueue.h
	$(CXX) $(CXXFLAGS) -c CharmQueue.cpp

LockLessQueue.o: LockLessQueue.cpp IQueue.h MPSCQueue.h
	$(CXX) $(CXXFLAGS) -c LockLessQueue.cpp

Benchmark.o: Benchmark.cpp IQueue.h Measurements.h
	$(CXX) $(CXXFLAGS) -c Benchmark.cpp

LockQueue.o: LockQueue.cpp IQueue.h
	$(CXX) $(CXXFLAGS) -c LockQueue.cpp

cleanall:
	rm -f *.o BenchmarkingModule *.txt *.png

cleanimg:
	rm -f *.png
