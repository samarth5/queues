/* Synopsis
Boost.Lockfree provides thread-safe and lock-free queues. It has two versions - general and spsc_queue (optimzed for one producer-one consumer access)

It is not implemented with a circular buffer, by default. If size exceeds capacity, it uses std::allocator to extend the queue ; can also create a queue of constant size => ensures lockfree behaviour 

Check if atomics are provided or not, if not, lock-free property would be lost

Caveat - Lockfree is a formal concept, while Lockless is about implementation
This means that lock-free algorithms are lockless but not the other way around

Using Boost directory (filtered just for using lockfree queue) given in MoodyCamel open sourced library 
*/

#include "IQueue.h"
#include "./Boost/lockfree/queue.hpp"
#include <iostream>

using namespace boost::lockfree;

template <class T>
class BoostQueue : public IQueue<T>
{
    private:
	// creating a fixed size queue, assumption  - maximum 65500 threads
        boost::lockfree::queue<T,fixed_sized<true>> queueInstance{65500};

    public:

        virtual void Enqueue(T elementToQueue)
        {
            queueInstance.push(elementToQueue);
        }

        virtual T Dequeue()
        {
            T frontElement;

	    if(queueInstance.pop(frontElement))
		return frontElement;
	    return NULL;

        }

        virtual IQueue<T>* GetInstance()
        {
            return new BoostQueue<T>();
        }
};
