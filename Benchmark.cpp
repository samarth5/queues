#include "Measurements.h"
#include "IQueue.h"

#include <algorithm>
#include <thread>
#include <chrono>
#include <numeric>
#include <assert.h>
#include <math.h>

using namespace std;
using namespace std::chrono;

template <class T>
class Benchmark
{
    private:
        
        void BindThreadToHardwareThread(thread& workerThread, int i)
        {
            // Create a cpu_set_t object representing a set of CPUs. Clear it and mark
            // only CPU i as set.
            cpu_set_t cpuset;
            CPU_ZERO(&cpuset);
            CPU_SET(i, &cpuset);
            int rc = pthread_setaffinity_np(workerThread.native_handle(), sizeof(cpu_set_t), &cpuset);

            if (rc != 0) 
            {
                cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";                
            }
        }

        double GetEnqueueBenchmarkSingle(Operation<T> operationToPerform)
        {
            IQueue<T>& baseQueue = *(operationToPerform.BaseQueuePointer->GetInstance());

            thread workers[operationToPerform.NumberOfThreads];

            auto start = high_resolution_clock::now();
            
            for (int i = 0; i < operationToPerform.NumberOfThreads; i++)
            {
                workers[i] = thread(operationToPerform.EnqueueOperation, ref(baseQueue),
                operationToPerform.NumberOfEnqueueRepititions);

                BindThreadToHardwareThread(workers[i], i);
            }

            for (int i = 0; i < operationToPerform.NumberOfThreads; i++)
            {
                workers[i].join();
            }

            auto end = high_resolution_clock::now();
            duration<double> diff = end-start;

            // assert(baseQueue.GetSize() == operationToPerform.NumberOfThreads * 
            //     operationToPerform.NumberOfEnqueueRepititions);

            return diff.count();
        }

        double GetEnqueueDequeueBenchmarkSingle(Operation<T> operationToPerform)
        {
            IQueue<T>& baseQueue = *(operationToPerform.BaseQueuePointer->GetInstance());

            thread workers[operationToPerform.NumberOfThreads];

            auto start = high_resolution_clock::now();
            
            for (int i = 0; i < operationToPerform.NumberOfThreads - 1; i++)
            {
                workers[i] = thread(operationToPerform.EnqueueOperation, ref(baseQueue),
                    operationToPerform.NumberOfEnqueueRepititions);

                BindThreadToHardwareThread(workers[i], i);
            }

            workers[operationToPerform.NumberOfThreads - 1] = thread(operationToPerform.DequeueOperation, 
                ref(baseQueue), operationToPerform.NumberOfDequeueRepititions, 
                operationToPerform.NumberOfMicroSecondsToDequeueFor);

            BindThreadToHardwareThread(workers[operationToPerform.NumberOfThreads - 1], 
                operationToPerform.NumberOfThreads - 1);

            for (int i = 0; i < operationToPerform.NumberOfThreads; i++)
            {
                workers[i].join();
            }    

            auto end = high_resolution_clock::now();
            duration<double> diff = end-start;
            // long totalDequeued = operationToPerform.NumberOfDequeueRepititions * operationToPerform.NumberOfMicroSecondsToDequeueFor;
            // long totalEnqueued = (operationToPerform.NumberOfThreads - 1) * operationToPerform.NumberOfEnqueueRepititions;
            // long totalLeft = totalDequeued >= totalEnqueued ? 0 : totalEnqueued - totalDequeued;

            // assert((long)baseQueue.GetSize() == totalLeft);

            return diff.count();            
        }

    public:        

        Measure GetBenchmarkMeasure(Operation<T> operationToPerform, bool onlyEnqueue, int numberOfMeasurements)
        {
            vector<double> measurements;
            while (numberOfMeasurements--)
            {
                if (onlyEnqueue)
                {
                    measurements.push_back(GetEnqueueBenchmarkSingle(operationToPerform));
                }
                else
                {
                    measurements.push_back(GetEnqueueDequeueBenchmarkSingle(operationToPerform));
                }
            }
                        
            return Measure(measurements);
        }
};
