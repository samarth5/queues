// Deriving from IQueue<char*> => LockLessQueue is not a class template
// Deviation from usual IQueue based queues => call create() first and foremost

#include "IQueue.h" 
#include "PCQueue.h"

using namespace std;


class CharmQueue : public IQueue<char*>
{
    private:
        PCQueue queueInstance;
    
    public:
        
        // To allocate memory dynamically
        // default constructor
        CharmQueue()
        {
            queueInstance = PCQueueCreate();
        } 

        virtual void Enqueue(char* elementToQueue)
        {
            PCQueuePush(queueInstance, elementToQueue);
            
        }
        // returns NULL if frontElement not found
        virtual char* Dequeue()
        {
            char* frontElement = PCQueuePop(queueInstance);
            return frontElement;
        }        

        virtual IQueue<char*>* GetInstance()
        {       
            return new CharmQueue();
        }
        
    
};
