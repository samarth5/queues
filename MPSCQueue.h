#ifndef MPSCQUEUE_H
#define MPSCQUEUE_H

#include "IQueue.h"
#include <stdlib.h>
#ifndef __cplusplus
#include <stdatomic.h>
#else
#include <atomic>
#define _Atomic(X) std::atomic<X>
#define atomic_uint std::atomic<unsigned int>
#define atomic_uintptr_t std::atomic<std::uintptr_t>
#endif
//#include <stdint.h>
#include <cstdint>
#include <limits.h>
#include <sched.h>
using namespace std;

typedef char** DataNode;

/* Queue Parameters - All must be 2^n for wrapping to work */
#define NodePoolSize 0x100
#define FreeNodeWrap (NodePoolSize - 1)

int DataNodeSize;
int MaxDataNodes;
int QueueUpperBound;
int DataNodeWrap;
int QueueWrap;
int messageQueueOverflow;

typedef struct CmiMemorySMPSeparation_t {
    unsigned char padding[128];
} CmiMemorySMPSeparation_t;

/*
 * FreeNodePoolStruct
 * Holds nodes that are no longer in use but are not being freed
 * Acts as a SPSC bounded queue
 */
typedef struct FreeNodePoolStruct
{
    atomic_uint push;
    atomic_uint pull;
    atomic_uintptr_t nodes[NodePoolSize];
}*FreeNodePool;

typedef struct MPSCQueueStruct
{
    atomic_uint push;
    CmiMemorySMPSeparation_t pad1;
    unsigned int pull;
    CmiMemorySMPSeparation_t pad2;
    atomic_uintptr_t *nodes;
    CmiMemorySMPSeparation_t pad3;
    FreeNodePool freeNodePool;
} *MPSCQueue;

        
static unsigned int WrappedDifference(unsigned int push, unsigned int pull)
{
    unsigned int difference;
    if(push < pull)
    {
        difference = (UINT_MAX - pull + push);
    }
    else
    {
        difference = push - pull;
    }
    return difference;
}

static int QueueFull(unsigned int push, unsigned int pull)
{
    int difference = WrappedDifference(push, pull);

    // The use of QueueUpperBound - 2*DataNodeSize is to remove the possiblity of wrapping
    // around the entire queue and pushing to a block that is going to be freed.
    // This removes concurrency issues with wrapping the entire queue and pushing to a block that
    // is being popped.
    if(difference >= (QueueUpperBound - 2*DataNodeSize)) 
        return 1;
    else
    	return 0;
}

/* Creates a DataNode while holds the char* to data */
static DataNode DataNodeCreate(void)
{
    DataNode node = (DataNode)malloc(sizeof(char*)*DataNodeSize);
    int i;
    for(i = 0; i < DataNodeSize; ++i) node[i] = NULL;
        return node;
}

/* Initialize the FreeNodePool as a SPSC queue */
static FreeNodePool FreeNodePoolCreate(void)
{
    FreeNodePool free_q;

    free_q = (FreeNodePool)malloc(sizeof(struct FreeNodePoolStruct));
    atomic_store_explicit(&free_q->push, (unsigned int)0, memory_order_relaxed);
    atomic_store_explicit(&free_q->pull, (unsigned int)0, memory_order_relaxed);

    int i;
    // CHANGE - altered uintptr_t => std::uintptr_t
    for(i = 0; i < NodePoolSize; ++i)
        atomic_store_explicit(&free_q->nodes[i], (std::uintptr_t)NULL, memory_order_relaxed);

    return free_q;
}

/* Clean up all data on the queue */
static void FreeNodePoolDestroy(FreeNodePool q)
{
    int i;
    for(i = 0; i < NodePoolSize; ++i)
    {
        if((std::uintptr_t)atomic_load_explicit(&q->nodes[i], memory_order_acquire) != (std::uintptr_t)NULL)
    	{
      	    DataNode n = (DataNode)atomic_load_explicit(&q->nodes[i], memory_order_acquire);
      	    free(n);
    	}
    }
    free(q);
}

static DataNode get_free_node(FreeNodePool q)
{
    DataNode node;
    unsigned int push;
    unsigned int pull = atomic_load_explicit(&q->pull, memory_order_acquire);

    // Claim the next unique pull value if the queue is not empty
    do
    {
        push = atomic_load_explicit(&q->push, memory_order_acquire);

    	if(pull == push) // Pool is empty, need to allocate a new DataNode.
      	    return DataNodeCreate();

    } while(!atomic_compare_exchange_weak_explicit(&q->pull, &pull, (pull + 1) & FreeNodeWrap, memory_order_release, memory_order_relaxed));

    // If the element is NULL, a producer is still pushing to the slot, but has begun the push operation.
    while((node = (DataNode)atomic_load_explicit(&q->nodes[pull], memory_order_acquire)) == NULL);
        atomic_store_explicit(&q->nodes[pull], (std::uintptr_t)NULL, memory_order_release); //NULL out spot in queue to indicate available spot.

    return node;
}

static void add_free_node(FreeNodePool q, DataNode available)
{
    unsigned int pull;
    unsigned int push = atomic_load_explicit(&q->push, memory_order_acquire);

    // Claim a unique push value if the queue is not empty
    do
    {
        // The next push value's element has not been NULL'd yet by a consumer, indicating the queue is full
    	if((std::uintptr_t)atomic_load_explicit(&q->nodes[push], memory_order_acquire) != (std::uintptr_t)NULL)
    	{
      	    free(available);
      	    return;
    	}

    } while(!atomic_compare_exchange_weak_explicit(&q->push, &push, (push + 1) & FreeNodeWrap, memory_order_release, memory_order_relaxed));

    atomic_store_explicit(&q->nodes[push], (std::uintptr_t)available, memory_order_release);
}

// CHANGE - removing static function type
MPSCQueue MPSCQueueCreate(void)
{
    /* Initialize the MPSCQueue struct */
    MPSCQueue Q = (MPSCQueue)malloc(sizeof(struct MPSCQueueStruct));
    Q->nodes = (atomic_uintptr_t*)malloc(sizeof(atomic_uintptr_t)*MaxDataNodes);
    Q->freeNodePool = FreeNodePoolCreate();
    Q->pull = 0;
    atomic_store_explicit(&Q->push, (unsigned int)0, memory_order_relaxed);

    unsigned int i;
    for(i = 0; i < MaxDataNodes; ++i)
    {
        atomic_store_explicit(&Q->nodes[i], (std::uintptr_t)NULL, memory_order_relaxed);
    }

    return Q;
}

static void MPSCQueueDestroy(MPSCQueue Q)
{
    /* Iterate through blocks. Every Datanode in the array must be freed before the node array */
    unsigned int i, j;
    for(i = 0; i < MaxDataNodes; ++i)
        if((std::uintptr_t)atomic_load_explicit(&Q->nodes[i], memory_order_acquire) != (std::uintptr_t)NULL)
    	{
      	    free((DataNode)atomic_load_explicit(&Q->nodes[i], memory_order_acquire));
    	}

    FreeNodePoolDestroy(Q->freeNodePool);
    free(Q->nodes);
    free(Q);
}

/* Index of DataNode in the node array */
static inline unsigned int get_node_index(unsigned int value)
{
    return ((value & QueueWrap) / DataNodeSize);
}

/* Gets the DataNode to push to */
static DataNode get_push_node(MPSCQueue Q, unsigned int push_idx)
{
    /* Index in the block: block[block_idx] */
    unsigned int node_idx = get_node_index(push_idx);

    /* If it is the first in the DataNode then create the node otherwise wait for it */
    if((push_idx & DataNodeWrap) == 0)
    {
	// CHANGE
	//cout << "& DataNodeWrap = 0" << endl;
        DataNode new_node = get_free_node(Q->freeNodePool);
    	atomic_store_explicit(&Q->nodes[node_idx], (std::uintptr_t)new_node, memory_order_release);
    	return new_node;
    }
    else // Wait until the producer with the first element in the DataNode creates the node.
    {
	//cout << "& DataNodeWrap != 0" << endl;
        DataNode node;
    	while((node = (DataNode)atomic_load_explicit(&Q->nodes[node_idx], memory_order_acquire)) == NULL);
    	return node;
    }
}

/* Get index of pop node in the popped block */
static inline DataNode get_pop_node(MPSCQueue Q, unsigned int pull_idx)
{
    unsigned int node_idx = get_node_index(pull_idx);

    return (DataNode)atomic_load_explicit(&Q->nodes[node_idx], memory_order_relaxed);
}

/* Check whether or not a node is ready to be freed */
static void check_mem_reclamation(MPSCQueue Q, unsigned int pull_idx, DataNode node)
{
    unsigned int node_idx = get_node_index(pull_idx);

    /* If we are pulling from the end of a node, free the node */
    if((pull_idx & DataNodeWrap) == (DataNodeSize - 1))
    {
        add_free_node(Q->freeNodePool, node);
    	atomic_store_explicit(&Q->nodes[node_idx], (std::uintptr_t)NULL, memory_order_relaxed);
    }
}

static int MPSCQueueEmpty(MPSCQueue Q)
{
    unsigned int push = atomic_load_explicit(&Q->push, memory_order_relaxed);
    unsigned int pull = Q->pull;
    return WrappedDifference(push, pull) == 0;
}

static int MPSCQueueLength(MPSCQueue Q)
{
    unsigned int push = atomic_load_explicit(&Q->push, memory_order_relaxed);
    unsigned int pull = Q->pull;
    return (int)WrappedDifference(push, pull);
}

static char* MPSCQueueTop(MPSCQueue Q)
{
    unsigned int pull = Q->pull;
    unsigned int push = atomic_load_explicit(&Q->push, memory_order_acquire);

    DataNode node = get_pop_node(Q, pull);

    if(pull == push || node == NULL) return NULL; // Queue is empty

    unsigned int node_pull = pull & DataNodeWrap;

    char* data = node[node_pull];
    return data;
}


static void MPSCQueuePush(MPSCQueue Q, char *data)
{
    unsigned int push = atomic_fetch_add_explicit(&Q->push, (unsigned int)1, memory_order_release);
    unsigned int pull = Q->pull;

    while(QueueFull(push, pull)) //Block until the push index is available to push to
    {
        //ReportOverflow();
	cout << "OverFlow" << endl;
    	sched_yield();
    	pull = Q->pull;
    }

    DataNode node = get_push_node(Q, push);
    node[push & DataNodeWrap] = data;
}

static char *MPSCQueuePop(MPSCQueue Q)
{
    unsigned int pull = Q->pull;
    unsigned int push = atomic_load_explicit(&Q->push, memory_order_acquire);
    if(pull == push) // If the queue is empty
        return NULL;

    DataNode node = get_pop_node(Q, pull);
    if(node == NULL) // If a producer has not finished allocating the block we are attempting to pop from
        return NULL;

    unsigned int node_pull = pull & DataNodeWrap;

    char * data = node[node_pull];
    if(data == NULL) // If a producer has not finished pushing an element we are attempting to pop
        return NULL;

    node[node_pull] = NULL; //NULL the element to indicate it is available again

    Q->pull = (pull + 1);

    check_mem_reclamation(Q, pull, node); //Check if we can free the node

    return data;
}

#endif
