#!/bin/bash
echo "Setting library path.."
export tbb_path="tbb-2018"
export tbb_bin="${PWD}/${tbb_path}" #
export LD_LIBRARY_PATH="${tbb_bin}:$LD_LIBRARY_PATH" #
echo "Cleaning.."
make cleanall
set -e
echo "Building Benchmark.."
make
echo "Running Benchmark.."
./BenchmarkingModule
