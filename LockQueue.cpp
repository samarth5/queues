#include "IQueue.h"
#include <queue>
#include <mutex>
#include <iostream>

using namespace std;

template <class T>
class LockQueue : public IQueue<T>
{
    private:
        mutex mutexInstance;
        queue<T> queueInstance;
        
    public:

        virtual void Enqueue(T elementToQueue)
        {
            lock_guard<mutex> lg(mutexInstance);
            queueInstance.push(elementToQueue);
        }

        virtual T Dequeue()
        {
            lock_guard<mutex> lg(mutexInstance);
            if (!queueInstance.empty())
            {
                T frontElement = queueInstance.front();
                queueInstance.pop();
                return frontElement;
            }
        }

        virtual IQueue<T>* GetInstance()
        {
            return new LockQueue<T>();
        }
};