import matplotlib.pyplot as plt


tickLabels = ["StlQueue", "PCQueue", "TbbQueue", "MoodyCamelQueue", "LockLessQueue"]

f, ax = plt.subplots(4, 2)
ir = 0
jr = 0

titles = []

fileValues = []

for i in range(1,9):
    with open(str(i) + ".txt") as fileHandle:
        valuesToPlot = []
        lines = fileHandle.readlines()
        titles.append(lines[0])
        for line in lines[2:]:
            details = line.split("\t")
            index = int(details[0])
            valuesToPlot.append(map(float, details[1].split(",")))
        fileValues.append(valuesToPlot)

numMeasures = 0

with open("numMeasures.txt") as fileHandle:
    numMeasures = int(fileHandle.readline())

r = 0
title_font = {'size':'8', 'color':'black', 'weight':'normal', 'verticalalignment':'center'}
for fileValue in fileValues:
    ax[ir, jr].boxplot(fileValue, showfliers = False)
    ax[ir, jr].set_title(titles[r].expandtabs(), title_font)
    r+=1
    ax[ir, jr].set_xticklabels(tickLabels)
    jr +=1 
    if jr >= 2:
        ir += 1
        jr = 0

plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.2, hspace=0.4)
plt.suptitle("Number of measurements: " + str(numMeasures))
plt.show()
