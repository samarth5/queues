/* Synopsis
(1) run script tbbvars.sh
(2) Need to link libtbb.so
 
*/

#include "IQueue.h"
#include "./tbb-2018/tbb/concurrent_queue.h"
#include <iostream>


template <class T>
class TbbQueue : public IQueue<T>
{
    private:
        tbb::concurrent_queue<T> queueInstance;

    public:
        
        virtual bool IsEmpty()
        {
            return queueInstance.empty();
        }

        virtual void Enqueue(T elementToQueue)
        {
            queueInstance.push(elementToQueue);
        }

        virtual T Dequeue()
        {
            T frontElement;

	    if(queueInstance.try_pop(frontElement))
		return frontElement;
	    return NULL;

        }

        virtual long GetSize()
        {
            // supports size method
	    // but with no guarantee for correctness
            return (long)queueInstance.unsafe_size();
        }

        virtual IQueue<T>* GetInstance()
        {
            return new TbbQueue<T>();
        }
};
